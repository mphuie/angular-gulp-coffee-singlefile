express = require 'express'
path = require 'path'
bodyparser = require 'body-parser'

app = express()
appPath = path.resolve __dirname, 'public'

app.use express.static appPath
app.use bodyparser.json()
app.use bodyparser.urlencoded { extended: false }

app.listen 8081
console.log 'Listening on port 8081'
