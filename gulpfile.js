var gulp = require('gulp');
var gutil = require('gulp-util');
var minify = require('gulp-minify');
var coffee = require('gulp-coffee');
var watch = require('gulp-watch');
var nodemon = require('gulp-nodemon');
var livereload = require('gulp-livereload');

gulp.task('coffee', function() {
  gulp
    .src('./src/*.coffee')
    .pipe(coffee({ bare: true}).on('error', gutil.log))
    .pipe(gulp.dest('./public/'));
})

gulp.task('watch', function() {
  gulp.watch('./src/*.coffee', ['coffee'])
})

gulp.task('server', function() {
  livereload.listen();
  nodemon({
    script: 'server.coffee',
    ext: 'coffee'
  })
})

gulp.task('default', ['server', 'watch'])
