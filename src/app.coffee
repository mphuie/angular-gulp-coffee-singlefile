app = angular.module 'myapp', ['ui.router', 'ui.bootstrap', 'toaster', 'ngAnimate']

app.config ['$stateProvider', '$urlRouterProvider', ($stateProvider, $urlRouterProvider) ->
  $urlRouterProvider.otherwise '/'
  $stateProvider
    .state 'home', {
      url: '/'
      templateUrl: '/partials/home.html'
      controller: 'MainCtrl'
    }
]

app.controller 'MainCtrl', ['$scope', '$http', 'toaster', ($scope, $http, toaster) ->
  # types: success, error, wait, warning, note
  toaster.pop 'success', 'title', 'example toast'
]
